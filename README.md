# lh-radiology-semi-automatic-image-annotator

lh-radiology-semi-automatic-image-annotator is a web app for ai guided medical data annotations.

![](images/bbox.gif)
![](images/segmentation.gif)

**Application Architecture diagram**

![](images/architecture.png)


## Installation

Running the app (assuming you have docker-compose [installed](https://docs.docker.com/compose/install/) )

download the models (retinanet, yolov3, unet)

[g drive link](https://drive.google.com/drive/folders/1nqw6SMBO_kNqzMpZzb07YcNiZF44EZm6?usp=sharing) for model download

put the folders in models/ (models should be in the root of this repo)

install [npmjs](https://www.npmjs.com/get-npm)

```bash
cd gui
```

```bash
npm install
```

```bash
npm run-script build
```
move the build directory to the backend folder

In the terminal go to the root of the repo and run:

```bash
docker-compose build
```
```bash
docker-compose up
```
URL for frontend: [local](http://127.0.0.1:10081)

URL for django admin panel: [local](http://127.0.0.1:10081/admin/)

## Documentation

**Django API Doc**

[link to Django docs](https://documenter.getpostman.com/view/11388923/SztHYkjm)

**Middleware doc generation**

```bash
pip install -r requirements.txt
```
```bash
cd docs
```
```bash
make html
```

## Model Metrics of chestXray data:

**Object_Detection:**

Disease | Atelectasis | Cardiomegaly | Effusion | Infiltrate | Mass | Nodule | Pneumonia | Pneumothorax 
--- | --- | --- | --- |--- |--- |--- |--- |---
Average Precision @ 0.5 IOU | 0.0024223323 | 0.48233068 | 0.0058859 | 0.03669084 | 0.15007754 | 0.01916758 | 0.011720786 | 0.00017587829 


Precision mAP @ 0.5IOU = 0.08855896

**Unet:**

Average IOU: 0.77378

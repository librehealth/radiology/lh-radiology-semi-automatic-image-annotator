"""This module is required for routing between the views"""
from django.urls import path, include, re_path
from .views import *

urlpatterns = [
    path('userinfo', UserProfileView.as_view()),
    path('add_xray', XrayCreateView.as_view()),
    path('show_xray', XrayListView.as_view()),
    path('update_bbox/<int:pk>', BboxUpdateView.as_view()),
    path('get_bbox/<int:pk>', BboxGetView.as_view()),
    path('get_dl_model', DLModelGetView.as_view()),
    path('get_csv', CSVView.as_view()),
    path('get_segment_csv', SegmentCSVView.as_view()),
    path('get_diseases', DiseaseGetView.as_view()),
    path('post_logs', LogsPostView.as_view()),
    path('segmentation', SegmentView.as_view())
]